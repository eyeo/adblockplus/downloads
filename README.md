# ⚠️ REPOSITORY DEPRECATED

This repository has been deprecated and is no longer actively maintained.

## New Location

All new releases and downloads are now available at:
https://gitlab.com/eyeo/extensions/extensions/-/releases

If you need to get the releases in a script you can use Gitlab's API: https://gitlab.com/api/v4/projects/59518842/releases